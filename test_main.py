import unittest
from main import Moradia, Apartamento


class MyTestCase(unittest.TestCase):
	def test_moradia(self):
		moradia1 = Moradia(100, 'Rua das Limeiras', 672, 250000)
		self.assertEqual(moradia1.__dict__, {'area': 100, 'endereco': 'Rua das Limeiras', 'numero': 672, 'preco_imovel': 250000, 'tipo': 'Não especificado'})
		self.assertEqual(moradia1.gerar_relatorio_preco(), 2500)

	def test_apartamento(self):
		ape = Apartamento(100, 'Rua das Limeiras', 672, 500000, 700, 110, 2)

		self.assertEqual(ape.__dict__, {'area': 100, 'endereco': 'Rua das Limeiras', 'numero': 672, 'preco_imovel': 500000, 'tipo': 'Apartamento', 'preco_condominio': 700, 'andar': 11, 'num_apt': 110, 'num_elevadores': 2})
		self.assertEqual(ape.gerar_relatorio(), 'Rua das Limeiras - apt 110 - andar: 11 - elevadores: 2 - preco por m²: R$ 5000')

if __name__ == '__main__':
	unittest.main()
