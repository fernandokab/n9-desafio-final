class Moradia:
	def __init__(self, area, endereco, numero, preco_imovel, tipo='Não especificado'):
		self.area = area
		self.endereco = endereco
		self.numero = numero
		self.preco_imovel = preco_imovel
		self.tipo = tipo

	def gerar_relatorio_preco(self):
		preco = self.preco_imovel / self.area
		return int(preco)

class Apartamento(Moradia):
	def __init__(self, area, endereco, numero, preco_imovel, preco_condominio, num_apt, num_elevadores):
		super().__init__(area, endereco, numero, preco_imovel, 'Apartamento')
		self.preco_condominio = preco_condominio
		self.num_apt = num_apt
		self.num_elevadores = num_elevadores
		self.andar = num_apt // 10

	def gerar_relatorio(self):
		return f"{self.endereco} - apt {self.num_apt} - andar: {self.andar} - elevadores: {self.num_elevadores} - preco por m²: R$ {self.gerar_relatorio_preco()}"
